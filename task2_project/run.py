import atexit
import os
import pickle
import smtplib
import sys
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from apscheduler.schedulers.background import BackgroundScheduler
from flask import Flask, request
import requests

SUBSCRIPTION_LIMIT = 5

app = Flask(__name__)

if os.path.isfile('lists_of_subscriptions.pickle'):
    with open('lists_of_subscriptions.pickle', 'rb') as pickle_file:
        lists_of_subscriptions = pickle.load(pickle_file)
else:
    lists_of_subscriptions = {}

def save_lists_of_subscriptions(lists_of_subscriptions) -> None:
    with open('lists_of_subscriptions.pickle', 'wb') as pickle_file:
        pickle.dump(lists_of_subscriptions, pickle_file)

@app.route('/subscription', methods=['POST', 'DELETE'])
def subscription() -> (str, int):
    if request.method == 'DELETE':
        if "email" not in request.args:
            return 'Email expected as parametr\n', 400
        email = request.args["email"]

        if "ticker" not in request.args:
            lists_of_subscriptions.pop(request.args["email"], None)
            save_lists_of_subscriptions(lists_of_subscriptions)
            return '', 200

        ticker = request.args["ticker"]

        if email not in lists_of_subscriptions:
            return 'No subscriptions found\n', 400
        email_lists_of_subscriptions = lists_of_subscriptions[email]
        email_lists_of_subscriptions[:] = [
            subscription for subscription in email_lists_of_subscriptions\
                if subscription["ticker"] != ticker]
        save_lists_of_subscriptions(lists_of_subscriptions)
        return '', 200

    for key in request.json:
        if key not in ('ticker', 'email', 'min_price', 'max_price'):
            return 'Invalid key: ' + key + '\n', 400

    if 'email' not in request.json:
        return 'Expected email value\n', 400

    email = request.json["email"]

    if type(email) != str:
        return 'Email should be string\n', 400

    if 'ticker' not in request.json:
        return 'Expected ticker value\n', 400

    ticker = request.json["ticker"]

    if type(ticker) != str:
        return 'Ticker should be string\n', 400

    if 'min_price' not in request.json and 'max_price' not in request.json:
        return 'Expected min_price or max_price value\n', 400
    for price in 'min_price', 'max_price':
        if price in request.json:
            try:
                request.json[price] = float(request.json[price])
            except ValueError:
                return price + ' should be a number\n', 400

    if email not in lists_of_subscriptions:
        lists_of_subscriptions[email] = []
    email_lists_of_subscriptions = lists_of_subscriptions[email]
    if len(email_lists_of_subscriptions) == 5:
        return 'Subscription limit already reached\n', 403
    email_lists_of_subscriptions.append(
        dict(
            {"ticker": ticker},
            **{
                key: request.json[key] for key in request.json.keys() & {
                    "min_price",
                    "max_price"
                }
            }
        )
    )
    save_lists_of_subscriptions(lists_of_subscriptions)
    return '', 200


def sent_mail(destination_address: str, ticker: str, text: str) -> None:
    message = MIMEMultipart()
    message["From"] = os.environ["EMAIL"]
    message["To"] = destination_address
    message["Subject"] = "Ticker " + ticker + " cost changed"
    message.attach(MIMEText(text, 'plain'))
    sent_mail.smtp.send_message(message)


def check_prices() -> None:
    def price(ticker: str) -> float:
        responce = requests.get(
            'https://www.alphavantage.co/query',
            params={
                'function': 'GLOBAL_QUOTE',
                'symbol': ticker,
                'apikey': check_prices.apikey
            }
        )
        responce.raise_for_status()
        return float(responce.json()["Global Quote"]["05. price"])


    global lists_of_subscriptions
    subscriptions_tickers = {
        subscription["ticker"]\
            for subscriptions in lists_of_subscriptions.values()\
            for subscription in subscriptions
    }
    try:
        price = {ticker: price(ticker) for ticker in subscriptions_tickers}
    except (KeyError, ValueError, requests.exceptions.RequestException) as exception:
        print("Can't get ticker price: ", str(exception), file=sys.stderr)
        return

    for email, subscriptions in lists_of_subscriptions.items():
        for subscription_index, subscription in enumerate(subscriptions):
            ticker = subscription["ticker"]
            ticker_price = price[ticker]

            if "min_price" in subscription:
                minimal_price = subscription["min_price"]
                if ticker_price < minimal_price:
                    sent_mail(
                        email,
                        ticker,
                        "Ticker " + ticker + " now costs " + str(ticker_price)\
                            + " (less than" + str(minimal_price) + ")")
                    subscription.pop("min_price")
            if "max_price" in subscription:
                maximal_price = subscription["max_price"]
                if ticker_price > maximal_price:
                    sent_mail(
                        email,
                        ticker,
                        "Ticker " + ticker + " now costs " + str(ticker_price)\
                            + " (more than" + str(maximal_price) + ")")
                    subscription.pop("max_price")

        # Remove empty subscriptions
        subscriptions[:] = [
            subscription for subscription in subscriptions\
                if "min_price" in subscription or "max_price" in subscription
        ]
    # Remove empty lists of subscriptions
    lists_of_subscriptions = {
        email: subscriptions for email, subscriptions in lists_of_subscriptions.items()\
                if subscriptions
    }
    save_lists_of_subscriptions(lists_of_subscriptions)


def main() -> None:
    for variable in "ALPHAVANTAGE_API_KEY", "SMTP_HOST", "EMAIL", "EMAIL_PASSWORD":
        if variable not in os.environ:
            print("Environment variable", variable, "not found", file=sys.stderr)
            return
    check_prices.apikey = os.environ["ALPHAVANTAGE_API_KEY"]


    sent_mail.smtp = smtplib.SMTP_SSL(os.environ["SMTP_HOST"])
    sent_mail.smtp.login(os.environ["EMAIL"], os.environ["EMAIL_PASSWORD"])
    scheduler = BackgroundScheduler()
    check_prices()
    scheduler.add_job(check_prices, 'interval', minutes=1)
    scheduler.start()
    atexit.register(scheduler.shutdown)
    atexit.register(sent_mail.smtp.quit)
    app.run()

if __name__ == '__main__':
    main()
