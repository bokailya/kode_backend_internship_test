import re

substitution_regex = re.compile('{[\S\s]*?}')

def phrase_search(object_list: list, search_string: str) -> int:
    """Find phrase in object_list"""
    for obj in object_list:
        slots = obj["slots"]
        phrase = obj["phrase"]

        parts = []
        previous_position = 0
        for match in substitution_regex.finditer(phrase):
            parts += (phrase[previous_position:match.start()], match.group())
            previous_position = match.end()
        if previous_position != len(phrase):
            parts.append(phrase[previous_position:])

        # True if found match phrase[:i] with first j parts
        dynamic_programming_table = [
            [False] * (len(parts) + 1) for i in range(len(search_string) + 1)
        ]
        dynamic_programming_table[0][0] = True
        for matched in range(0, len(search_string) + 1):
            for part_index, part in enumerate(parts, 1):
                if len(part) and (part[0] != '{' or part[-1] != '}'):
                    part_start = matched - len(part)
                    if part_start >= 0 and search_string.startswith(
                        part,
                        part_start
                    ) and dynamic_programming_table[part_start][part_index - 1]:
                        dynamic_programming_table[matched][part_index] = True
                    continue
                for slot in (part[1:-1],) + tuple(slots):
                    slot_start = matched - len(slot) 
                    if slot_start >= 0 and search_string.startswith(
                        slot,
                        slot_start
                    ) and dynamic_programming_table[slot_start][part_index - 1]:
                        dynamic_programming_table[matched][part_index] = True
                        break
        if dynamic_programming_table[-1][-1]:
            return obj["id"]
    return 0


if __name__ == "__main__":
    """ 
    len(object) != 0
    object["id"] > 0
    0 <= len(object["phrase"]) <= 120
    0 <= len(object["slots"]) <= 50
    """
    object = [
        {"id": 1, "phrase": "Hello world!", "slots": []},
        {"id": 2, "phrase": "I wanna {pizza}", "slots": ["pizza", "BBQ", "pasta"]},
        {"id": 3, "phrase": "Give me your power", "slots": ["money", "gun"]},
        {"id": 17, "phrase": "aa{ab}{ab}ca{bc}", "slots": ["a", "b"]},
        {"id": 4, "phrase": "{a}aa{bab}cabc{}", "slots": ["c", "d"]},
    ]

    assert phrase_search(object, 'I wanna pasta') == 2
    assert phrase_search(object, 'Give me your power') == 3
    assert phrase_search(object, 'Hello world!') == 1
    assert phrase_search(object, 'I wanna nothing') == 0
    assert phrase_search(object, 'Hello again world!') == 0
    assert phrase_search(object, 'I need your clothes, your boots & your motorcycle') == 0
    assert phrase_search(object, 'aaababcabc') == 17
    assert phrase_search(object, 'aaaacaa') == 17
    assert phrase_search(object, 'aaabacab') == 17
    assert phrase_search(object, 'aaaccabcd') == 4
    assert phrase_search(object, 'caadcabc') == 4
    assert phrase_search(object, 'aadcabc') == 0
